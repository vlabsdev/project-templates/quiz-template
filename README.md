# QUIZ - Activity

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

-  Clone this repository using Command Line Interface (CLI) on your system
-  Switch to `developer` branch
-  Add the activity files under the `activity` folder.
-  the activity folder should have one `questions.xlsx` in its root . ie : activity/questions.xlsx
-  other dependent files and folders should also be in the activity folder only.
-  Push the code to the `developer` branch

## Hosting / Deploying your activity.
-  After adding acitivity , raise a Merge Request (MR) from `developer`branch to the `main` branch.
-  `Our team will verify the MR and merge to main. `
-  Once the build pipeline passes (~10-15 mins), it will be `auto-deployed` under the url `https://jigyasa-csir.in/nodal-acronym/activity-id/ `
-  for eg: if the `nodal acronym : cgcri` and the `activity-id: n12-t1-a2`then the deployment URL will be  `https://jigyasa-csir.in/cgcri/n12-t1-a2/`

